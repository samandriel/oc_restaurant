<?php namespace Altuz\RestarantManager\Models;

use Model;

/**
 * Model
 */
class FoodItem extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $belongsToMany = [
        'categories' => [
            Category::class,
            'table' => 'altuz_restaurantmanager_foods_categories',
            'key'      => 'food_item_id',
            'otherKey' => 'category_id'
        ]
    ];

    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'altuz_restarantmanager_foods';
}
