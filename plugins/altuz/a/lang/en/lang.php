<?php return [
    'plugin' => [
        'name' => 'Restarant Manager',
        'description' => 'Description',
    ],
    'name' => 'Name',
    'category' => 'Category',
    'select_category' => 'Please select category',
    'description' => 'Description',
    'image' => 'Image',
    'ref_code' => 'Reference Code #',
    'foods' => 'Foods',
    'slug' => 'Slug',
];