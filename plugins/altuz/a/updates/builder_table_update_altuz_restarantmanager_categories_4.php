<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestarantmanagerCategories4 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restarantmanager_categories', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restarantmanager_categories', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
