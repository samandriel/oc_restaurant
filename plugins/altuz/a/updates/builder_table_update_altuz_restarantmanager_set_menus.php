<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestarantmanagerSetMenus extends Migration
{
    public function up()
    {
        Schema::table('altuz_restarantmanager_set_menus', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restarantmanager_set_menus', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
