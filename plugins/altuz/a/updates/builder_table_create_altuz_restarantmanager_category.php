<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestarantmanagerCategory extends Migration
{
    public function up()
    {
        Schema::create('altuz_restarantmanager_category', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 100);
            $table->string('description', 250);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restarantmanager_category');
    }
}
