<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestarantmanagerCategories extends Migration
{
    public function up()
    {
        Schema::rename('altuz_restarantmanager_category', 'altuz_restarantmanager_categories');
    }
    
    public function down()
    {
        Schema::rename('altuz_restarantmanager_categories', 'altuz_restarantmanager_category');
    }
}
