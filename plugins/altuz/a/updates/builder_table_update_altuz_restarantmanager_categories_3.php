<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestarantmanagerCategories3 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restarantmanager_categories', function($table)
        {
            $table->string('ref_code', 20)->nullable();
            $table->string('description', 250)->nullable()->change();
            $table->string('image', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restarantmanager_categories', function($table)
        {
            $table->dropColumn('ref_code');
            $table->string('description', 250)->nullable(false)->change();
            $table->string('image', 255)->nullable(false)->change();
        });
    }
}
