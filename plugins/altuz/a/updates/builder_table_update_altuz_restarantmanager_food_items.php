<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestarantmanagerFoodItems extends Migration
{
    public function up()
    {
        Schema::rename('altuz_restarantmanager_foods', 'altuz_restarantmanager_food_items');
    }
    
    public function down()
    {
        Schema::rename('altuz_restarantmanager_food_items', 'altuz_restarantmanager_foods');
    }
}
