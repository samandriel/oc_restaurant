<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestarantmanagerFoodCategory extends Migration
{
    public function up()
    {
        Schema::create('altuz_restarantmanager_foods_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('category_id');
            $table->integer('food_item_id');
            $table->primary(['category_id','food_item_id'], 'altuz_category_id_food_item_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restarantmanager_foods_categories');
    }
}