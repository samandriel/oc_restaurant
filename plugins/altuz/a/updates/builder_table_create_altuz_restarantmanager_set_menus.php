<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestarantmanagerSetMenus extends Migration
{
    public function up()
    {
        Schema::create('altuz_restarantmanager_set_menus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 100);
            $table->string('description', 300)->nullable();
            $table->string('food_items');
            $table->string('ref_code');
            $table->decimal('price', 10, 0);
            $table->boolean('published')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restarantmanager_set_menus');
    }
}
