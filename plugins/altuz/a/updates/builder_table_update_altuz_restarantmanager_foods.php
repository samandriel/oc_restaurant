<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestarantmanagerFoods extends Migration
{
    public function up()
    {
        Schema::table('altuz_restarantmanager_foods', function($table)
        {
            $table->string('ref_code', 20)->nullable();
            $table->string('description', 250)->nullable()->change();
            $table->string('image', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restarantmanager_foods', function($table)
        {
            $table->dropColumn('ref_code');
            $table->string('description', 250)->nullable(false)->change();
            $table->string('image', 255)->nullable(false)->change();
        });
    }
}
