<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestarantmanagerFoods extends Migration
{
    public function up()
    {
        Schema::create('altuz_restarantmanager_foods', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 140);
            $table->string('description', 250);
            $table->integer('category')->unsigned();
            $table->string('image');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restarantmanager_foods');
    }
}
