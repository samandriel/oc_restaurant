<?php namespace Altuz\RestarantManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestarantmanagerFoods3 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restarantmanager_foods', function($table)
        {
            $table->dropColumn('category');
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restarantmanager_foods', function($table)
        {
            $table->integer('category')->unsigned();
        });
    }
}
