<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestaurantSetMenus extends Migration
{
    public function up()
    {
        Schema::create('altuz_restaurant_set_menus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('slug');
            $table->string('image')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restaurant_set_menus');
    }
}
