<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestaurantSetMenus2 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restaurant_set_menus', function($table)
        {
            $table->string('ref_code');
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restaurant_set_menus', function($table)
        {
            $table->dropColumn('ref_code');
        });
    }
}
