<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestaurantCategories extends Migration
{
    public function up()
    {
        Schema::table('altuz_restaurant_categories', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->renameColumn('image_url', 'image');
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restaurant_categories', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->renameColumn('image', 'image_url');
        });
    }
}
