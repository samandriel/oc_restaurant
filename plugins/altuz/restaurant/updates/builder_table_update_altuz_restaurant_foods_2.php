<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestaurantFoods2 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restaurant_foods', function($table)
        {
            $table->decimal('price', 10, 0);
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restaurant_foods', function($table)
        {
            $table->dropColumn('price');
        });
    }
}
