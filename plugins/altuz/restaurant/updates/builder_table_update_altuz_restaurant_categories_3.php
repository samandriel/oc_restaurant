<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestaurantCategories3 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restaurant_categories', function($table)
        {
            $table->dropColumn('price');
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restaurant_categories', function($table)
        {
            $table->decimal('price', 10, 0);
        });
    }
}
