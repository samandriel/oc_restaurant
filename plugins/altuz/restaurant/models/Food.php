<?php namespace Altuz\Restaurant\Models;

use Model;

/**
 * Model
 */
class Food extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    public $belongsToMany = [
        'categories' => [
            Category::class,
            'table'    => 'altuz_restaurant_foods_categories',
            'key'      => 'food_id',
            'otherKey' => 'category_id'
        ],
        'setMenus' => [
            SetMenu::class,
            'table'    => 'altuz_restaurant_foods_set_menus',
            'key'      => 'food_id',
            'otherKey' => 'set_menu_id'
        ]
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'altuz_restaurant_foods';
}
