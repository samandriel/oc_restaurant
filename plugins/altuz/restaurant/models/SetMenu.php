<?php namespace Altuz\Restaurant\Models;

use Model;

/**
 * Model
 */
class SetMenu extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    public $belongsToMany = [
        'foods' => [
            Food::class,
            'table'    => 'altuz_restaurant_foods_set_menus',
            'key'      => 'set_menu_id',
            'otherKey' => 'food_id'
        ]
    ];
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'altuz_restaurant_set_menus';
}
